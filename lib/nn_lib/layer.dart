import 'neuron.dart';
import 'utils.dart' as utils;

class Layer {
  List<Neuron> neurons = [];
  double Function(double)? activation;

  // Constructor for input layers
  Layer(List<double> inputs,int numOfMemory) {
    for (int i=0 ; i< inputs.length ; i++) {
      this.neurons.add(Neuron(value: inputs[i],bias: 1, theta: utils.randomWeight(
          -2.4 / (inputs.length - numOfMemory), 2.4 / (inputs.length-numOfMemory))  ));
    }
  }

  // Constructor for hidden & output layers
  Layer.hidden(
    int numberOfNeurons, /// عدد العصبون بطبقة  5
    int weightsPerNeuron,  /// الاوزان الداخلة
    double bias,
    this.activation, /// عباخد اينم
  ) {


    for (int i = 0; i < numberOfNeurons; i++) {
      List<double> weights = [];
      for (var j = 0; j < weightsPerNeuron; j++) {
        weights.add(utils.randomWeight(
            -2.4 / weightsPerNeuron, 2.4 / weightsPerNeuron));
      }
      final theta =
          utils.randomWeight(-2.4 / weightsPerNeuron, 2.4 / weightsPerNeuron);

      this.neurons.add(Neuron.hidden(weights: weights, bias: bias, theta: theta));
    }
  }

}
