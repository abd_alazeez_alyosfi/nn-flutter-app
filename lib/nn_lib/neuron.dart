class Neuron {
  // Static Neuron Attributes
  static double minWeight = 0;
  static double maxWeight = 0;

  List<double> weights = [];
  List<double> newWeights = [];
  double theta;
  double? newTheta;
  double? errorG;
  double bias;
  double? value;
  double? newValue = 0;

  Neuron({required this.value, required this.bias, required this.theta});

  Neuron.hidden({required this.weights, required this.bias, required this.theta}) {
    this.weights = weights;
    this.newWeights = this.weights;
    this.errorG = 0.0;
    this.newTheta = this.theta;
  }

  Neuron.memory(this.weights, this.bias, this.theta) {
    this.newWeights = this.weights;
    this.errorG = 0.0;
    this.newTheta = this.theta;
    this.value = 0;
    this.newValue = 0;
  }

  void updateWeights() {
    this.weights = this.newWeights;
    this.theta = this.newTheta!;
  }

  @override
  String toString() {
    return '''minWeight : ${minWeight},
maxWeight : ${maxWeight},
weights : ${weights},
newWeights : ${newWeights},
theta : ${theta},
newTheta : ${newTheta},
errorG : ${errorG},
bias : ${bias},
value : ${value},
newValue : ${newValue},''';

    return super.toString();
  }
}
