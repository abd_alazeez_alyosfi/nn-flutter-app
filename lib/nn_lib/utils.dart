import 'dart:math' as math;

double randomWeight(double min, double max) {
  math.Random rand = new math.Random();
  return rand.nextDouble() * (max - min) + min;
}

double sigmoid(double x) {
  return (1 / (1 + math.pow(math.e, -1 * x))).toDouble();
}

double purelin(double x) {
  // return (1 / (1 + math.pow(math.e, -1 * x))).toDouble();
return x ;
  // return double.parse(x.toStringAsFixed(5));
}

double tansig(double x) {
  return 2 / (((1 + math.exp((x.isNaN || x.isInfinite )? 10 : (-2 * x))) - 1));
}

double rms(List<double> output, double target) {
  double sum = 0;
  for (int i = 0; i < 1; i++) {
    sum += (math.pow(output[i] - target, 2));
  }

  return math.sqrt(sum / output.length);
}
