class Pair {
  List<double> inputData = [];
  double target;

  Pair(this.inputData, this.target){
    assert(target>=0||target<=1);
  }
}

class DataSet {
  List<Pair> pairs = [];
  /// حجم داتا التدريب
   int? _trainLength;
  /// حجم الخرج عنا
   late int outPutLength;
  int? inPutLength;

  List<Pair> getTrainData() {
    /// منشان اخدداتا للتدريب
    return pairs.sublist(0, _trainLength);
  }

  List<Pair> getTestData() {
    /// منشان اخد داتا التيست
    return pairs.sublist(_trainLength!);
  }

  setTrainLength() {
    final dataLength = pairs.length;
    return _trainLength = (dataLength * (80 / 100)).toInt();
  }

  setOutAndInLength() {
    /// منشان احدد حجم الخرج والدخل
    outPutLength = 1;
    inPutLength = pairs.first.inputData.length;
  }

  addPair(Pair pair) {
    pairs.add(pair);
  }
}
