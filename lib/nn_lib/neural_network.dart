import 'package:flutter/cupertino.dart';
import 'package:collection/collection.dart';
import '../cahr_screen.dart';
import 'dataset.dart';
import 'layer.dart';
import 'neuron.dart';
import 'utils.dart' as utils;

enum NeuralNetworkType {
  elman,
  normal,
}

enum Activation {
  sigmoid,
  tansig,
  purelin,
}

class NeuralNetwork {
  final int features;
  List<double>? _memory;
  List<Layer> layers = [];
  final NeuralNetworkType type;

  NeuralNetwork({required this.features, this.type = NeuralNetworkType.normal}) {
    /// بحجز طبقة للدخل
    layers.add(Layer([
      /// ليش ماحدد عصبونات دخل
      /// لانو بل فورورد باخدن
    ], 0));
  }

  void addLayer({required int neuronsCount, required Activation activation}) {
    int wightsCount;

    if (layers.length == 1) {
      wightsCount = features;

      /// منشان اضافة عصبونات بطبقة
      if (NeuralNetworkType.elman == type) {
        /// افتراضيا الاوزان اصفار للطبقة الاولى
        _memory = List.filled(neuronsCount, 0.0);
        wightsCount += neuronsCount;
      }
    }

    /// النوع تاني حصرا
    else

      /// اخر طبقة قبل مااضيف باخد العدد الاوزان الداخلة فولي كونيكت
      wightsCount = layers.last.neurons.length;
    layers.add(Layer.hidden(neuronsCount, wightsCount, 1, _getActivation(activation)));
  }

  double Function(double) _getActivation(Activation activation) {
    switch (activation) {
      case Activation.sigmoid:
        return utils.sigmoid;
      case Activation.tansig:
        return utils.tansig;
      case Activation.purelin:
        return utils.purelin;
      default:
        return utils.sigmoid;
    }
  }

  summery() {
    int i = 1;
    for (Layer layer in layers.sublist(1)) {
      print('Layer($i)  , activation: ${layer.activation}, neurons: ${layer.neurons.length}');
      int j = 1;
      layer.neurons.forEach((element) {
        print('Neuron($j) weights: ${element.weights}');
        j++;
      });
      i++;
    }
  }
}

void forward(NeuralNetwork nn, List<double> inputs) {
  /// بلزمني بس الدخل ليش الخرج مابلزم هيك قال حسام

  //If type is elman, add memory values to input layer
  if (nn.type == NeuralNetworkType.elman) {
    nn.layers[0] = Layer(List.from(inputs)..addAll(nn._memory ?? []), nn._memory?.length ?? 0);
  } else {
    nn.layers[0] = Layer(inputs, 0);
  }
  // Forward propagation
  for (int i = 1; i < nn.layers.length; i++) {
    //تمرر عبى جميع الطبقات
    // Starts from 1st hidden layer
    for (int j = 0; j < nn.layers[i].neurons.length; j++) {
      double sum = (0.0);
      for (int k = 0; k < nn.layers[i - 1].neurons.length; k++) {
        sum += ((nn.layers[i - 1].neurons[k].value! * nn.layers[i].neurons[j].weights[k]));
      }
      sum += (((nn.layers[i].neurons[j].bias) * (nn.layers[i].neurons[j].theta)));
      var value = nn.layers[i].activation!(sum.toDouble())/4;
      nn.layers[i].neurons[j].value = value;
    }
  }

  /// عبرجع من الخرج للطبقة الاولى كونتكست
  if (nn.type == NeuralNetworkType.elman) {
    nn._memory = nn.layers[1].neurons.map((e) => e.value!).toList();
  }
}

double? backward(NeuralNetwork nn, double learningRate, Pair pair) {
  ///  موئر طبقة الخرج
  int outputLayerIndex = nn.layers.length - 1;

  /// اعلاقتو تحت الجذر مجمو تاريغت  معيار للمقياس التشابه تقريبا
  double rms;
  // Update the output layers

  /// قيم الخرج منشان احسب ارمس
  List<double> output = nn.layers[outputLayerIndex].neurons.map((e) => e.value!).toList();

  /// انا بكل باك ورد برجع rms
  rms = utils.rms(output, pair.target);

  for (int i = 0; i < nn.layers[outputLayerIndex].neurons.length; i++) {
    // For each output
    final double output = nn.layers[outputLayerIndex].neurons[i].value!;
    final double target = pair.target;
    final double outputError = target - output;

    // if (outputError.abs() < 0.001) {
    //   return -1;
    // }
    final double errorG = (output * (1 - output)) * outputError;
    nn.layers[outputLayerIndex].neurons[i].errorG = errorG;
    for (int j = 0; j < nn.layers[outputLayerIndex].neurons[i].weights.length; j++) {
      final double previousOutput = nn.layers[outputLayerIndex - 1].neurons[j].value!; //edit -1
      final double deltaWeight = errorG * previousOutput * learningRate;

      nn.layers[outputLayerIndex].neurons[i].newWeights[j] =
          nn.layers[outputLayerIndex].neurons[i].weights[j] + deltaWeight;
    }

    final double error = errorG * nn.layers[outputLayerIndex].neurons[i].bias;
    nn.layers[outputLayerIndex].neurons[i].newTheta =
        nn.layers[outputLayerIndex].neurons[i].theta + (learningRate * error);
  }

  // Update all the subsequent hidden layers
  for (int i = outputLayerIndex - 1; i > 0; i--) {
    // Backward
    for (int j = 0; j < nn.layers[i].neurons.length; j++) {
      // For all neurons in that layers
      double output = nn.layers[i].neurons[j].value!;
      double gradientSum = sumGradient(nn, j, i + 1);
      double errorG = (gradientSum) * (output * (1 - output));
      nn.layers[i].neurons[j].errorG = errorG;

      for (int k = 0;
          k <
              ((i == 1 && nn.type == NeuralNetworkType.elman)
                  ? (nn.layers[i].neurons[j].weights.length - (nn._memory?.length ?? 0))
                  : nn.layers[i].neurons[j].weights.length);
          k++) {
        // And for all their weights
        double previousOutput = nn.layers[i - 1].neurons[k].value!;
        double error = errorG * previousOutput;
        nn.layers[i].neurons[j].newWeights[k] =
            nn.layers[i].neurons[j].weights[k] + (learningRate * error);
      }
      final double error = errorG * nn.layers[i].neurons[j].bias;
      nn.layers[i].neurons[j].newTheta = nn.layers[i].neurons[j].theta + (learningRate * error);
    }
  }

  // Update all the weights
  for (int i = 1; i < nn.layers.length; i++) {
    for (int j = 0; j < nn.layers[i].neurons.length; j++) {
      nn.layers[i].neurons[j].updateWeights();
    }
  }
  return rms;
}

// This function sums up all the gradient connecting a given neuron in a given layer
double sumGradient(NeuralNetwork nn, int nIndex, int lIndex) {
  //n == current  , l ==  output
  // n= current && l = next
  double gradientSum = 0;
  final Layer currentLayer = nn.layers[lIndex];
  for (int i = 0; i < currentLayer.neurons.length; i++) {
    final Neuron currentNeuron = currentLayer.neurons[i];
    gradientSum += currentNeuron.weights[nIndex] * currentNeuron.errorG!;
  }
  return gradientSum;
}

// This function is used to train
List<List<double>> train(
    NeuralNetwork nn, DataSet dt, int iteration, double learningRate, double _) {
  final trainPairs = dt.getTrainData();
  final printPoint = (iteration * 0.02).toInt();
  final List<double> charDD = [];
  final List<double> loss = [];
  for (int i = 0; i < iteration; i++) {
    if (nn.type == NeuralNetworkType.elman) {
      nn._memory = List.filled(nn._memory?.length ?? 0, 0.0);
    }
    int count = 0;
    for (int j = 0; j < trainPairs.length; j++) {
      forward(nn, trainPairs[j].inputData);
      // rmsSum += backward(nn, learningRate, trainPairs[j])!;
      // if
      var los = (backward(nn, learningRate, trainPairs[j]) ?? 0);
      if (i == iteration - 1) {
        loss.add(los);
      }
      // {
      //   return charDD;
      // }
      if (nn.layers[nn.layers.length - 1].neurons.first.value?.round() == trainPairs[j].target) {
        count++;
      }
    }
    final rms = count / trainPairs.length;
    charDD.add(rms);

    if ((i + 1) % printPoint == 0 || i == iteration - 1) {
      print('iteration: ${i + 1}');
      print('rms: $rms');
    }
  }
  return [charDD, loss];
}

// double computeRms(NeuralNetwork nn, List<double> output, List<double> target) {
//   return utils.rms(output, target);
// }
