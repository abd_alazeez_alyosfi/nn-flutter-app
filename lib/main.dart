import 'dart:io';
import 'dart:math' as math;
import 'package:csv/csv.dart';
import 'package:cupertino_setting_control/cupertino_setting_control.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:neu_flutter/cahr_screen.dart';
import 'package:neu_flutter/nn_lib/dataset.dart';
import 'package:neu_flutter/nn_lib/neural_network.dart';
import 'package:neu_flutter/nn_lib/utils.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter NN',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'NN'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String _searchAreaResult = 'NN';

  TextEditingController featuresCon = TextEditingController();
  List<Widget> _neuNumberOfHidden = [];
  List<TextEditingController> _textEditingNumHiddenCon = [];
  List<TextEditingController> _activationFunCon = [];
  List<Widget> _activationFun = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Directionality(
        textDirection: TextDirection.rtl,
        child: ListView(
          shrinkWrap: true,
          children: [
            SettingRow(
              rowData: SettingsDropDownConfig(
                  title: 'نوع الشبكة ',
                  initialKey: _searchAreaResult,
                  choices: {'NN': 'NN', 'Elman': 'Elman',},
                  onDropdownFinished: () {}),
              onSettingDataRowChange: (data) {
                setState(() {
                  _searchAreaResult = data;
                });
              },
              config: const SettingsRowConfiguration(
                  showAsTextField: true, showTitleLeft: false, showAsSingleSetting: false),
            ),
            SizedBox(
              height: 10,
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Card(
                  elevation: 0,
                  shape: BeveledRectangleBorder(borderRadius: BorderRadius.only(topLeft: Radius.circular(15))),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ListTile(
                        title: Text('عدد الميزات  ',
                            style: TextStyle(
                              color: CupertinoColors.systemBlue,
                              fontWeight: FontWeight.bold,
                              fontSize: 15.0,
                            )),
                        subtitle: TextField(
                          controller: featuresCon,
                          keyboardType: TextInputType.number,
                        )),
                  )),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Card(
                  elevation: 0,
                  shape: BeveledRectangleBorder(borderRadius: BorderRadius.only(topLeft: Radius.circular(15))),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ListTile(
                        title: Text('عدد الطبقات المخفية  ',
                            style: TextStyle(
                              color: CupertinoColors.systemBlue,
                              fontWeight: FontWeight.bold,
                              fontSize: 15.0,
                            )),
                        subtitle: TextField(
                          onChanged: hiddenChange,
                          keyboardType: TextInputType.number,
                        )),
                  )),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Card(
                  elevation: 0,
                  shape: BeveledRectangleBorder(borderRadius: BorderRadius.only(topLeft: Radius.circular(15))),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ListTile(
                        title: Text('عدد العصبونات   ',
                            style: TextStyle(
                              color: CupertinoColors.systemBlue,
                              fontWeight: FontWeight.bold,
                              fontSize: 15.0,
                            )),
                        subtitle: Row(
                          children: [
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Column(
                                  children: [Text(" في كل طبقة"), ..._neuNumberOfHidden],
                                ),
                              ),
                            ),
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Column(
                                  children: [Text("تابع التفعيل"), ..._activationFun],
                                ),
                              ),
                            ),
                          ],
                        )),
                  )),
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _runNN,
        tooltip: 'run',
        child: Icon(Icons.play_arrow_outlined),
      ),
    );
  }

  hiddenChange(data) {
    if (data.isNotEmpty && isNumeric(data)) {
      _neuNumberOfHidden.clear();
      _textEditingNumHiddenCon.clear();
      _activationFun.clear();
      _activationFunCon.clear();
      for (int i = 0; i < int.parse(data); ++i) {
        _textEditingNumHiddenCon.add(TextEditingController());
        _activationFunCon.add(TextEditingController());
        int n = _textEditingNumHiddenCon.length;
        int nFun = _activationFunCon.length;

        final textFiled = TextField(
          controller: _textEditingNumHiddenCon[n - 1],
          keyboardType: TextInputType.number,
        );
        _neuNumberOfHidden.add(textFiled);

        final textFiledFun = TextField(
          controller: _activationFunCon[nFun - 1],
          keyboardType: TextInputType.number,
        );
        _activationFun.add(textFiledFun);
      }
    } else {
      _neuNumberOfHidden.clear();
      _activationFunCon.clear();
      _textEditingNumHiddenCon.clear();
      _activationFun.clear();
    }
    setState(() {});
  }

  bool isNumeric(String s) {
    if (s == null) {
      return false;
    }
    return double.tryParse(s) != null;
  }

  Activation checkFun(int value) {
    if (value == 0) {
      return Activation.sigmoid;
    } else if (value == 1)
      return Activation.tansig;
    else
      return Activation.purelin;
  }

  Future<DataSet> loadDataSet(int inputDim) async {
    FilePickerResult? csv1 =
    await FilePicker.platform.pickFiles(type: FileType.custom, allowedExtensions: ['csv'], allowMultiple: false);
    final csv = await File(csv1!.files.first.path!).readAsString();
    // final csv = await rootBundle.loadString('assets/PayPal.csv');

    /// برجع ليستة ليستات
    var data = CsvToListConverter(eol: '\n', shouldParseNumbers: false).convert(csv);
    List<double> temp = [];
    double max = 0;
    // print(data[0]);
    for (int i = 0; i < data[0].length ; i++) {
      temp = [];
      for (int j = 0; j < data.length; j++) {
        temp.add(double.parse(data[j][i]));
      }
      max = temp.reduce(math.max);
      for (int j = 0; j < data.length; j++) {
        data[j][i] = (temp[j] / (max));
      }
    }

    print("----- data-------");
    // for(int i=0 ;i <  data.length ;i++){
    //   data[i][6] = data[i][6] > 0.035 ?1.0 :0.0;
    //   print(data[i][6]);
    // }
    print("----- data-------");

    // print(data[0]);

    DataSet dataSet = DataSet();

    data.forEach((element) {
      final outPut = element.sublist(inputDim, element.length);

      /// باخد جزء وجزء للدخل والخرج
      element.removeRange(inputDim, element.length);

      /// ضفت داخل والخرج عداتا سيت
      dataSet.addPair(Pair(element.map((e) => e is double ? e : double.parse(e)).toList(),
          outPut.first is double ? outPut.first : double.parse(outPut.first)));
    });

    /// منحدد حجم التدريب للبيرات
    dataSet.setTrainLength();

    dataSet.setOutAndInLength();

    /// محرمات !!!!!!!!!!!!!!!!!!!!!
    dataSet.pairs.shuffle();
    return dataSet;
  }

  Future<void> _runNN() async {
    List<double> charDD = [];
    List<double> loss = [];
    int features;

    ///  هون بتبلش الشبكة  بنعمل ر ن
    ///  ///حجم الدخل
    features = int.parse(featuresCon.text);
    DataSet dataSet = await loadDataSet(features);

    /// عباحد داتا سيت من
    /// مشنان الاوزان المجالات
    // Neuron.setRange(-2.4 / features, 2.4 / features);

    /// عرفت الشبكة

    NeuralNetworkType type = NeuralNetworkType.normal;
    if (_searchAreaResult == 'Elman') {
      type = NeuralNetworkType.elman;
    }

    ///  العادي
    NeuralNetwork nn = NeuralNetwork(features: features, type: type);

    for (int i = 0; i < _neuNumberOfHidden.length; ++i) {
      nn.addLayer(
        neuronsCount: int.parse(_textEditingNumHiddenCon[i].text),
        activation: checkFun(int.parse(_activationFunCon[i].text)),
      );
    }

    nn.addLayer(
      neuronsCount: dataSet.outPutLength,
      activation: type == NeuralNetworkType.elman ? Activation.purelin : Activation.sigmoid,
    );

    /// منشان التدريب
    /// بعطيها الشبكة
    /// الداتا سيت
    /// 1000 يبوك
    /// 000001  الخطأ المسموح
    print(" ---------------------   Start Train   -----------------------");
    List<List<double>> d = train(nn, dataSet, 500, 0.001, 0.001);
    charDD = d[0];
    loss = d[1];

    //I/flutter (19996): accuracy :0.7102731819930742
    final testPairs = dataSet.getTestData();

    int count = 0;
    print(" ---------------------   Start Test   -----------------------");

    for (var i in testPairs) {
      forward(nn, i.inputData);
      // print(i.target);
      // print(nn.layers.last.neurons.map((e) => e.value).toList());
      for (int index = 0; index < (dataSet.outPutLength); index++) {
        final neuron = nn.layers.last.neurons[index];
        // if (neuron.value!.round() != i.target[index]) break;
        print("${neuron.value}  ,   ${i.target}  , ${sigmoid(neuron.value!)}");
        if (nn.type == NeuralNetworkType.normal) {
          if (neuron.value! >= 0.4 && 1 == i.target) {
            count++;
          } else if (neuron.value! < 0.4 && 0 == i.target) {
            count++;
          }
        } else {
          if ((neuron.value! - i.target).abs() <= 0.05) {
            count++;
          }
          // if (neuron.value! >= 0.03&&  i.target >= 0.03) {
          //   count++;
          // } else if (neuron.value! < 0.1 && 0 == i.target) {
          //   count++;
          // }
        }

        ///there is mistake
        // if (index == (dataSet.outPutLength) - 1)
      }
    }

    print("test Params ");
    print(testPairs.length);
    print(count);
    print("accuracy :${count / testPairs.length}");
    print("loss $loss");
    print("loss $charDD");
    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) {
          print("Dadasdas");
          return CharScreen(
              loss:loss,
              accuracy: charDD,
              finalAccuracy: count / testPairs.length,
            );
        }));
  }
}
