import 'package:d_chart/d_chart.dart';
import 'package:flutter/material.dart';
import 'package:collection/collection.dart';

class CharScreen extends StatelessWidget {
  const CharScreen({Key? key, required this.accuracy, required this.finalAccuracy, required this.loss})
      : super(key: key);
  final List<double> accuracy;
  final List<double> loss;
  final double finalAccuracy;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Result"),
        actions: [
          IconButton(
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(builder: (_) {
                  return DetailsTrain(data: accuracy);
                }));
              },
              icon: Icon(Icons.show_chart)),
          IconButton(
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(builder: (_) {
                  return LossScreen(loss: loss,);
                }));
              },
              icon: Icon(Icons.error))
        ],
      ),
      body: Column(
        children: [
          Text(
            "Test accuracy : $finalAccuracy",
            style: Theme.of(context).textTheme.bodyMedium,
          ),
          SizedBox(
            height: 40,
          ),
          Text("Accuracy :  ", style: Theme.of(context).textTheme.bodyMedium),
          Expanded(
            child: DChartLine(
              data: [
                {
                  'id': 'Line',
                  'data': accuracy.mapIndexed((i, e) => {'domain': i + 1, 'measure': e * 100}).toList()
                    ..add({'domain': accuracy.length, 'measure': finalAccuracy * 50}),
                },
              ],
              lineColor: (lineData, index, id) => index == accuracy.length - 2 ? Colors.green : Colors.amber,
            ),
          ),
        ],
      ),
    );
  }
}

class DetailsTrain extends StatelessWidget {
  const DetailsTrain({Key? key, required this.data}) : super(key: key);
  final List<double> data;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("details train data")),
      body: ListView.builder(
        itemCount: data.length,
        itemBuilder: (context, index) {
          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 12),
            child: Row(
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8),
                  child: CircleAvatar(
                    child: Text((index + 1).toString()),
                  ),
                ),
                Text((data[index]).toString()),
              ],
            ),
          );
        },
      ),
    );
  }
}

class LossScreen extends StatelessWidget {
  const LossScreen({Key? key, required this.loss}) : super(key: key);
  final List<double> loss;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Loss screen ")),
      body: Column(children: [
        SizedBox(
          height: 40,
        ),
        Text("Loss:  ", style: Theme.of(context).textTheme.bodyMedium),
        Expanded(
          child: DChartLine(
            data: [
              {
                'id': 'Line',
                'data': loss.mapIndexed((i, e) => {'domain': i + 1, 'measure': e*100}).toList(),
              },
            ],
            lineColor: (lineData, index, id) => Colors.red,
          ),
        ),
      ]),
    );
  }
}
